import isAuth from '@/middleware/isAuth';
import axios from 'axios';


//  **************
//  Version Config
//  **************
const UI_VERSION = '1.0.0';
const API_VERSION = '1.0.0';

//  **************
//  baseUrl
//  **************
//  API_URL ON DEVELOP
let API_URL = 'http://127.0.0.1:8000/api';


//  **************
//  API Service
//  **************
const apiService = axios.create({
  baseURL: API_URL,
  headers: {
    'Authorization': 'Basic JOl14hhlf0ia0W1fo4tlBZIBPv1WNuYtnSfD6oPF2piw8HYXuGokuTvA97PX24eWh9cgJrOvBC6mE1QgNyNbjEWQnAqF4MgtLRClLO644h4NtEo50W2MtNWuhex5JHC8',
  }
});


// apiService.interceptors.request.use((config) => {
//     return config;
//   },
//   (error) => {
//     if (403 === error.response.status) {
//       window.$nuxt.$router.push('/')
//     }
//     return Promise.reject(error);
//   });
//
// apiService.interceptors.response.use((response) => {
//   return response;
// }, (error) => {
//   if (403 === error.response.status) {
//     window.$nuxt.$router.push('/')
//   }
//   return Promise.reject(error);
// });


export {apiService, UI_VERSION, API_VERSION, isAuth}
