export default {
  // 'auth/isMobile': state => state.device,
  // 'auth/hostURL': state => state.host,
  'auth/getProfile': state => state.profile,
  'auth/getLocalData': state => state.checkLocalData,
  'auth/getPermission': state => state.permission
}
