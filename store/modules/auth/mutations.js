export default {
  // AUTH_SET_HOST (state, payload) {
  //   state.host = payload.host
  // },
  // AUTH_SET_DEVICE (state, payload) {
  //   state.device = payload.device
  // },
  AUTH_SET_LOGIN (state) {
    state.isAuthen = true
  },
  AUTH_SET_LOGOUT (state) {
    state.isAuthen = false
    state.profile = {}
    state.permission = {}
  },
  // AUTH_SET_UPDATE (state, data) {
  //   state.auth = data
  // },
  AUTH_SET_PROFILE (state, profile) {
    state.profile = profile
  },
  AUTH_SET_PERMISSION (state, permission) {
    state.permission = permission
  },
  initialiseStore (state) {
    if (localStorage.getItem('store')) {
      state = JSON.parse(localStorage.getItem('store') || '')
    }
  },
  AUTH_SET_ADD_STORE (state) {
    localStorage.setItem('store', JSON.stringify(state))
  },
  AUTH_CHECK_LOCAL_DATA (state) {
    state.checkLocalData = true
  }
}
