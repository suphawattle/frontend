export default {
  // hostURL ({ commit }, state) {
  //   commit('AUTH_SET_HOST', { host: state.host })
  // },
  // isMobile ({ commit }, state) {
  //   commit('AUTH_SET_DEVICE', { device: state.device })
  // },
  login ({ commit }) {
    commit('AUTH_SET_LOGIN')
  },
  logout ({ commit }) {
    commit('AUTH_SET_LOGOUT')
    localStorage.removeItem('store')
    // if (Cookies.get('profiles')) {
    //   Cookies.remove('profiles')
    // }
    const timer = setTimeout(() => {}, 0)
    for (let i = parseInt(timer); i > 0; i--) {
      window.clearInterval(i)
      window.clearTimeout(i)
      if (window.cancelAnimationFrame) {
        window.cancelAnimationFrame(i)
      }
    }
  },
  // nuxtServerInit ({ commit }, { req }) {
  //   const accessToken = null
  //   console.log(req.headers)
  //   if (req.headers.cookie) {
  //     const parsed = cookieparser.parse(req.headers.cookie)
  //     accessToken = JSON.parse(parsed.auth)
  //   }
  //   commit('AUTH_SET_UPDATE', accessToken)
  // },
  setProfile ({ commit }, profile) {
    if (profile.status && profile.userStatusId === 1) {
      commit('AUTH_SET_LOGIN')
      commit('AUTH_SET_PROFILE', profile)
      commit('AUTH_SET_ADD_STORE')
    }
  },
  setPermission ({ commit }, permission) {
    commit('AUTH_SET_PERMISSION', permission)
  },
  checkLocalData ({ commit }) {
    commit('AUTH_CHECK_LOCAL_DATA')
  }
}
