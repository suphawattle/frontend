import Vuex from 'vuex'
// import login from './modules/login'
import authModule from './modules/auth'

export default () => new Vuex.Store({
  stateFactory: true,
  namespaced: true,
  modules: {
    auth: authModule
  }
})
