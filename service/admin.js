import {apiService, isAuth} from '@/config/config'

export default {
  // public access
  getDataDashboard: data => {
    return apiService.get('/admin/dashboard');
  },
  getListShopApprove: data => {
    return apiService.get('/admin/shop');
  },
  ApproveShopRequest: data => {
    return apiService.post('/admin/shop', data, {headers: isAuth()});
  },
  getListShopAll: data => {
    return apiService.get('/admin/all_shop');
  },
  RemoveShop: data => {
    return apiService.post('/admin/shop/' + data.id);
  },
  getDetailShopByID: data => {
    return apiService.get('/admin/shop/detail/' + data.id);
  },


  getAllUser: data => {
    return apiService.get('/admin/user');
  },
  RemoveUser: data => {
    return apiService.post('/admin/user/delete/' + data.id);
  },

  AdminLogin: data => {
    return apiService.post(`/admin/login`, data, {headers: isAuth()})
  },

  getListContact: data => {
    return apiService.get('/admin/contact');
  },
  getDetailContactByID: data => {
    return apiService.get('/admin/contact/edit' + data.id);
  },
  ReplyContact: data => {
    return apiService.post('/admin/contact/send_mail', data, {headers: isAuth()});
  },
  RemoveContact: data => {
    return apiService.post('/admin/contact/delete' + data.id);
  },

  getListReport: data => {
    return apiService.get('/admin/report');
  },
  getDetailReportByID: data => {
    return apiService.get('/admin/report/edit' + data.id);
  },
  ReplyReport: data => {
    return apiService.post('/admin/report/send_mail', data, {headers: isAuth()});
  },
  RemoveReport: data => {
    return apiService.post('/admin/report/delete' + data.id);
  },


  getTransactionSellerWeekly: data => {
    return apiService.get('/admin/seller/weekly');
  },
  getTransactionSellerMonthly: data => {
    return apiService.get('/admin/seller/monthly');
  },
  getTransactionSellerYear: data => {
    return apiService.get('/admin/seller/year');
  },
  getTransactionPaidWeekly: data => {
    return apiService.get('/admin/paid/weekly');
  },
  getTransactionPaidMonthly: data => {
    return apiService.get('/admin/paid/monthly');
  },
  getTransactionPaidYear: data => {
    return apiService.get('/admin/paid/year');
  },

  getAdvertise: data => {
    return apiService.get('/admin/advertise');
  },
  getAdvertiseDetail: data => {
    return apiService.get('/admin/advertise/detail/' + data.id);
  },
  getBanner: data => {
    return apiService.get('/admin/banner');
  },
  getBannerUpdate: data => {
    return apiService.post('/admin/banner/update', data, {headers: isAuth()});
  },

  getListWithdraw: data => {
    return apiService.get('/admin/withdraw');
  },
  approveWithdraw: data => {
    return apiService.get('/admin/withdraw/approve' + data.id);
  },
  rejectWithdraw: data => {
    return apiService.get('/admin/withdraw/reject' + data.id);
  },

  UpdateAdminName: data => {
    return apiService.post('/admin/update', data, {headers: isAuth()});
  },
  UpdateAdminPassword: data => {
    return apiService.post('/admin/password/update', data, {headers: isAuth()});
  },

  getListNotify: data => {
    return apiService.get('/admin/notification');
  },
  IsReadNotify: data => {
    return apiService.get('admin/notification/read' + data.id);
  },

  getExportExcelDashboard: data => {
    return apiService.get('/document/dashboard');
  },
}
