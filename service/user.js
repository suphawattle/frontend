import {apiService, isAuth} from '@/config/config'

export default {
  // public access
  test: data => {
    return apiService.get(`/healthcheck`)
  },
  UserLogin: data => {
    return apiService.post(`/login`, data)
  },
  UserLogout: async () => {
    // return apiService.get('/staff/logout', {
    //   headers: isAuth()
    // });
  },
  UserRegister: data => {
    return apiService.post(`/register`, data)
  },
  // UserRegister: data => {
  //   return apiService.post(`/user`, data, {headers: isAuth()})
  // },
  getUserData: data => {
    return apiService.get('/user/' + data.id, {headers: isAuth()});
  },
  UpdateUserProfile: data => {
    return apiService.post('/user/update', data, {headers: isAuth()})
  },
  UpdateUserProfileImage: data => {
    return apiService.post('/user/image', data, {headers: isAuth()})
  },

  ResetPassword: data => {
    return apiService.post('/forgetPassword', data, {headers: isAuth()})
  },
}
