export default function ({route, store, redirect}) {
  if (!store.state.user.isLogin) {
    return redirect('/')
  }
}
