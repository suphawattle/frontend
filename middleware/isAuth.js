export default function ({store}) {
  if (store.state.user.userToken) {
    return {
      'Authorization': 'Bearer ' + store.state.user.userToken,
    }
  } else {
    return {
      'Authorization': 'Bearer ',
    }
  }
}
